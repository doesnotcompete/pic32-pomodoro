#![no_main]
#![no_std]

use core::{fmt::Write, panic::PanicInfo};

use embedded_hal::{
    blocking::delay::DelayMs, digital::v2::*, prelude::_embedded_hal_blocking_delay_DelayUs,
    serial::Read, spi::Mode,
};
use max7219::{connectors::Connector, MAX7219};
use mips_rt::{entry, interrupt};
use pic32_config_sector::pic32mx2xx::*;
use pic32_hal::{
    clock::Osc,
    coretimer::{Delay, Timer},
    gpio::GpioExt,
    int,
    pac::{
        self, pps,
        tmr2::pr2::{self, PR2_W},
        INT,
    },
    pps::{output::Outputs, MapPin, NoPin, PpsExt},
    spi::Spi,
    time::U32Ext,
    uart::Uart,
};

// PIC32 configuration registers for PIC32MX1xx and PIC32MX2xx
#[cfg(any(feature = "pic32mx1xxfxxxb", feature = "pic32mx2xxfxxxb"))]
#[link_section = ".configsfrs"]
#[used]
pub static CONFIGSFRS: ConfigSector = ConfigSector::default()
    .FVBUSONIO(FVBUSONIO::OFF)
    .FUSBIDIO(FUSBIDIO::OFF)
    .IOL1WAY(IOL1WAY::OFF)
    .PMDL1WAY(PMDL1WAY::OFF)
    .FPLLIDIV(FPLLIDIV::DIV_2)
    .FPLLMUL(FPLLMUL::MUL_20)
    .FPLLODIV(FPLLODIV::DIV_2)
    .FNOSC(FNOSC::FRCPLL)
    .FSOSCEN(FSOSCEN::OFF)
    .FPBDIV(FPBDIV::DIV_1)
    .FWDTEN(FWDTEN::OFF)
    .JTAGEN(JTAGEN::OFF)
    .ICESEL(ICESEL::ICS_PGx1)
    .build();

const POMODORO_TIME: u32 = 1500;

#[entry]
fn main() -> ! {
    let p = pac::Peripherals::take().unwrap();
    let vpins = p.PPS.split();

    // setup clock control object
    let sysclock = 40_000_000_u32.hz();
    let clock = Osc::new(p.OSC, sysclock);
    let mut timer = Delay::new(sysclock);

    let partsa = p.PORTA.split();
    let partsb = p.PORTB.split();
    let mut led = partsb.rb5.into_push_pull_output();

    // Set up PWM
    let mut speaker = partsb.rb7.into_push_pull_output();
    let btn = partsb.rb8.into_pull_up_input();

    partsa
        .ra2
        .into_push_pull_output()
        .map_pin(vpins.outputs.sdo1);
    let spi = Spi::spi1(
        p.SPI1,
        pic32_hal::spi::Proto::Spi(Mode {
            polarity: embedded_hal::spi::Polarity::IdleLow,
            phase: embedded_hal::spi::Phase::CaptureOnFirstTransition,
        }),
        8,
    );
    let cs = partsb.rb4.into_push_pull_output();
    let mut display = MAX7219::from_spi_cs(4, spi, cs).unwrap();
    display.power_on().unwrap();
    // write given octet of ASCII characters with dots specified by 3rd param bits

    timer.delay_ms(10u32);

    loop {
        set_intensity(&mut display, 0x0C);
        count_up(&mut display, &btn, &mut led, &mut timer);
        ready(&mut display, &btn, &mut led, &mut timer);
        led.set_low();
        pomodoro(&mut display, &btn, &mut timer);
        led.set_high();
        beep(&mut speaker, &mut timer);
        overtime(&mut display, &btn, &mut timer);
    }
}

fn beep<O: OutputPin>(speaker: &mut O, timer: &mut Delay) {
    let mut on = false;
    for i in 1..50 {
        for j in 1..100 {
            if on {
                speaker.set_high();
                timer.delay_ms(1)
            } else {
                speaker.set_low();
                timer.delay_ms(1);
            }
            on = !on;
        }
        timer.delay_ms(5);
    }
}

fn set_intensity<T: Connector>(mut display: &mut MAX7219<T>, intensity: u8) {
    display.set_intensity(0, intensity).unwrap();
    display.set_intensity(1, intensity).unwrap();
    display.set_intensity(2, intensity).unwrap();
    display.set_intensity(3, intensity).unwrap();
}

fn count_up<T: Connector, I: InputPin, O: OutputPin>(
    mut display: &mut MAX7219<T>,
    button: &I,
    led: &mut O,
    timer: &mut Delay,
) {
    set_intensity(&mut display, 0x06);
    let mut on = false;
    let mut deciseconds = 0;
    loop {
        if deciseconds >= 10 && check_low(button, display) {
            break;
        }
        if deciseconds % 10 == 0 {
            if on {
                led.set_low();
            } else {
                led.set_high();
            }
            on = !on;
        }
        write_time(&mut display, &(deciseconds / 10), false);
        timer.delay_ms(100u32);
        deciseconds += 1;
    }
}

fn ready<T: Connector, I: InputPin, O: OutputPin>(
    mut display: &mut MAX7219<T>,
    button: &I,
    led: &mut O,
    timer: &mut Delay,
) {
    set_intensity(&mut display, 0x01);
    write_digits(&mut display, &[b' ', b' ', b' ', b' '], false);
    timer.delay_ms(2000u32);

    write_time(&mut display, &POMODORO_TIME, false);
    let mut on = false;
    let mut count = 0;
    loop {
        if check_low(button, display) {
            break;
        }
        if count >= 5 {
            if on {
                led.set_low();
            } else {
                led.set_high();
            }
            count = 0;
        }
        on = !on;
        count += 1;
        timer.delay_ms(100u32);
    }
}

fn pomodoro<T: Connector, I: InputPin>(
    mut display: &mut MAX7219<T>,
    button: &I,
    timer: &mut Delay,
) {
    set_intensity(&mut display, 0x06);
    let mut seconds: u32 = 0;
    let mut cancel_count = 0;
    while seconds <= POMODORO_TIME {
        if seconds >= 1 && check_low(button, &mut display) {
            if cancel_count >= 3 {
                write_digits(&mut display, b"!!!!", false);
                timer.delay_ms(1000u32);
                break;
            }
            cancel_count += 1;
        } else {
            cancel_count = 0;
        }
        write_time(&mut display, &(POMODORO_TIME - seconds), false);
        timer.delay_ms(1000u32);
        seconds += 1;
    }
}

fn overtime<T: Connector, I: InputPin>(
    mut display: &mut MAX7219<T>,
    button: &I,
    timer: &mut Delay,
) {
    set_intensity(&mut display, 0x0C);
    let mut milliseconds: u32 = 1000;
    loop {
        if check_low(button, display) {
            break;
        }
        write_time(&mut display, &(milliseconds / 1000), true);
        timer.delay_ms(200u32);
        milliseconds += 200;
    }
}

fn check_low<I: InputPin, T: Connector>(button: &I, mut display: &mut MAX7219<T>) -> bool {
    match button.is_low() {
        Ok(low) => low,
        Err(_) => {
            write_digits(display, b"!!!!", false);
            loop {}
        }
    }
}

fn write_time<T: Connector>(mut display: &mut MAX7219<T>, seconds: &u32, negative: bool) {
    let mut digits = [b' ', b' ', b' ', b' '];
    // Get minutes
    let mut minutes = seconds / 60;
    let mut seconds_in_minute = seconds % 60;

    for i in (2..4).rev() {
        if seconds_in_minute > 0 {
            digits[i] = (seconds_in_minute % 10) as u8;
            seconds_in_minute /= 10;
        } else {
            // Add padding
            digits[i] = 0;
        }
    }
    for i in (0..2).rev() {
        if minutes > 0 {
            digits[i] = (minutes % 10) as u8;
            minutes /= 10;
        } else {
            digits[i] = 0;
        }
    }
    write_digits(&mut display, &digits, negative);
}

fn write_digits<T: Connector>(display: &mut MAX7219<T>, digits: &[u8; 4], negative: bool) {
    for (i, digit) in digits.iter().enumerate() {
        let raw = match i {
            0 if negative => add_minus(&format_character(*digit)),
            1 => add_dots(&format_character(*digit)),
            _ => *format_character(*digit),
        };
        display.write_raw(i, &raw);
    }
}

fn format_character(character: u8) -> &'static [u8; 8] {
    #[rustfmt::skip]
    const DIGIT_1: &[u8; 8] = &[
        0b00001100,
        0b00010100,
        0b00100100,
        0b00000100,
        0b00000100,
        0b00000100,
        0b00000100,
        0b00000100,
    ];
    #[rustfmt::skip]
    const DIGIT_2: &[u8; 8] = &[
        0b00011100,
        0b00100100,
        0b00100100,
        0b00000100,
        0b00111000,
        0b00100000,
        0b00100000,
        0b00111100,
    ];
    #[rustfmt::skip]
    const DIGIT_3: &[u8; 8] = &[
        0b00111100,
        0b00000100,
        0b00000100,
        0b00111100,
        0b00111100,
        0b00000100,
        0b00000100,
        0b00111100,
    ];
    #[rustfmt::skip]
    const DIGIT_4: &[u8; 8] = &[
        0b00100100,
        0b00100100,
        0b00100100,
        0b00111100,
        0b00000100,
        0b00000100,
        0b00000100,
        0b00000100,
    ];
    #[rustfmt::skip]
    const DIGIT_5: &[u8; 8] = &[
        0b00111100,
		0b00100000,
		0b00100000,
		0b00111100,
		0b00111100,
		0b00000100,
		0b00000100,
        0b00111100,
    ];
    #[rustfmt::skip]
    const DIGIT_6: &[u8; 8] = &[
        0b00111100,
		0b00100000,
		0b00100000,
		0b00111100,
		0b00111100,
		0b00100100,
		0b00100100,
        0b00111100,
    ];
    #[rustfmt::skip]
    const DIGIT_7: &[u8; 8] = &[
        0b00111100,
		0b00000100,
		0b00000100,
		0b00000100,
		0b00000100,
		0b00000100,
		0b00000100,
        0b00000100,
    ];
    #[rustfmt::skip]
    const DIGIT_8: &[u8; 8] = &[
        0b00111100,
		0b00100100,
		0b00100100,
		0b00111100,
		0b00111100,
		0b00100100,
		0b00100100,
        0b00111100,
    ];
    #[rustfmt::skip]
    const DIGIT_9: &[u8; 8] = &[
        0b00111100,
		0b00100100,
		0b00100100,
		0b00111100,
		0b00111100,
		0b00000100,
		0b00000100,
        0b00111100,
    ];
    #[rustfmt::skip]
    const DIGIT_0: &[u8; 8] = &[
        0b00111100,
		0b00100100,
		0b00100100,
		0b00100100,
		0b00100100,
		0b00100100,
		0b00100100,
        0b00111100,
    ];
    #[rustfmt::skip]
    const MARK: &[u8; 8] = &[
        0b00011000,
		0b00011000,
		0b00011000,
		0b00011000,
		0b00011000,
		0b00000000,
		0b00011000,
        0b00011000,
    ];
    const BLANK: &[u8; 8] = &[0, 0, 0, 0, 0, 0, 0, 0];

    match character {
        1 | b'1' => DIGIT_1,
        2 | b'2' => DIGIT_2,
        3 | b'3' => DIGIT_3,
        4 | b'4' => DIGIT_4,
        5 | b'5' => DIGIT_5,
        6 | b'6' => DIGIT_6,
        7 | b'7' => DIGIT_7,
        8 | b'8' => DIGIT_8,
        9 | b'9' => DIGIT_9,
        0 | b'0' => DIGIT_0,
        b' ' => BLANK,
        _ => MARK,
    }
}

fn add_dots(src: &[u8; 8]) -> [u8; 8] {
    let mut dst = src.clone();
    dst[2] |= 0b00000001;
    dst[5] |= 0b00000001;
    dst
}

fn add_minus(src: &[u8; 8]) -> [u8; 8] {
    let mut dst = src.clone();
    dst[4] |= 0b11100000;
    dst[3] &= 0b00011111;
    dst[4] &= 0b11101111;
    dst[5] &= 0b00011111;
    dst
}

#[panic_handler]
fn panic(_panic_info: &PanicInfo<'_>) -> ! {
    loop {}
}
